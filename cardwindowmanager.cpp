/*
    This file is part of the KontingaCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
 */

#include "cardwindowmanager.hpp"

// program
#include "cardwindow.hpp"
// Akonadi Contacts
#include <Akonadi/Contact/ContactSearchJob>
// KF
#include <KWindowSystem>
// Qt
#include <QTimer>
#include <QApplication>

static const int ShutDownTime = 30 * 1000;
static const char uidPropertyId[] = "kontinga_contactuid";

namespace Kontinga {

CardWindowManager::CardWindowManager()
{
    m_shutDownTimer = new QTimer(this);
    m_shutDownTimer->setSingleShot(true);
    m_shutDownTimer->setInterval(ShutDownTime);
    connect(m_shutDownTimer, SIGNAL(timeout()),
            qApp, SLOT(quit()));

//     Book = StdAddressBook::self();

//     connect( Book, SIGNAL(addressBookChanged(AddressBook*)), SLOT(updateCards()) );

//     connect( Services::self(), SIGNAL(changed()), SLOT(updateCards()) );

    m_shutDownTimer->start();

    // demo: show any first contact
    Akonadi::ContactSearchJob* job = new Akonadi::ContactSearchJob(this);
    job->setQuery(Akonadi::ContactSearchJob::Email, "konqui@kde.org");
    connect(job, &KJob::result, this, &CardWindowManager::contactSearchResult);
}

CardWindowManager::~CardWindowManager()
{
    // QDictIterator<CardWindow> it(m_cards);
    // for( ; it.current(); ++it )
}

void CardWindowManager::showCard(const QString& uid)
{
    m_shutDownTimer->stop();

    CardWindow* card = m_cards.value(uid);

    // non already there?
    if (card) {
        KWindowSystem::setOnDesktop(card->winId(), KWindowSystem::currentDesktop());
        card->raise();
        return;
    }

    if (m_pendingContactQueries.contains(uid)) {
        return;
    }

    m_pendingContactQueries.insert(uid);
    Akonadi::ContactSearchJob* job = new Akonadi::ContactSearchJob(this);
    job->setQuery(Akonadi::ContactSearchJob::ContactUid, uid);
    job->setProperty(uidPropertyId, uid);

    connect(job, &KJob::result, this, &CardWindowManager::contactSearchResult);
}

void CardWindowManager::contactSearchResult(KJob* job)
{
    const QString uid = job->property(uidPropertyId).toString();
    m_pendingContactQueries.remove(uid);

    if (job->error() != 0) {
        // error handling, see job->errorString()
        return;
    }

    auto searchJob = qobject_cast<Akonadi::ContactSearchJob*>(job);

    const KContacts::Addressee person = searchJob->contacts().value(0);
    if (!person.isEmpty()) {
        auto card = new CardWindow(person);
        m_cards.insert(uid, card);
        connect(card, &CardWindow::closed,
                this, &CardWindowManager::removeCard);
        card->show();
    }
}

void CardWindowManager::closeCard(const QString& uid)
{
    Q_UNUSED(uid);
    // m_cards.remove( Person.uid() );
}

void CardWindowManager::updateCards()
{
    auto it = m_cards.begin();
    while (it != m_cards.end()) {
        const KContacts::Addressee person;// = Book->findByUid( it.current()->person().uid() );

        // no longer existant?
        if (person.isEmpty()) {
            delete it.value();
            it = m_cards.erase(it);
        } else {
            ++it;
        }
    }
}

void CardWindowManager::removeCard(const KContacts::Addressee& person)
{
    m_cards.remove(person.uid());

    if (m_cards.isEmpty()) {
        m_shutDownTimer->start();
    }
}

}
