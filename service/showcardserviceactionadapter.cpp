/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "showcardserviceactionadapter.hpp"

// Kontinga Core
#include <Kontinga/Roles>
// KF
#include <KLocalizedString>
// Qt
#include <QIcon>

QVariant ShowCardServiceActionAdapter::data(int role) const
{
    QVariant result;

    switch (role)
    {
    case Kontinga::DisplayTextRole:
        result = m_name.isEmpty() ?
                 i18n("Show &Card...") :
                 i18n("Show Card of %1...", m_name);
        break;
    case Kontinga::DisplayIconRole:
        result = QIcon("identity");
        break;
    case Kontinga::EnabledRole:
        result = true;
        break;
    default:
        ;
    }

    return result;
}
