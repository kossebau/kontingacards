/*
    This file is part of the KontingaCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
 */

#include "program.hpp"

// program
#include "cardwindowmanager.hpp"
#include "cardwindowmanagerdbusadaptor.hpp"
// KF
// #include <kuniqueapplication.h>
// #include <dcopclient.h>
// KF
#include <KDBusService>
#include <KLocalizedString>
#include <KCrash>
// Qt
#include <QIcon>
#include <QDBusConnection>
#include <QCommandLineParser>
#include <QDebug>

KontingaCardsProgram::KontingaCardsProgram(int argc, char* argv[])
    : m_serverApp(argc, argv)
{
    KAboutData::setApplicationData(m_aboutData);
    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("addressbook")));

    KCrash::initialize();
}

int KontingaCardsProgram::execute()
{
#if 0
    QCommandLineParser parser;
    m_aboutData.setupCommandLine(&parser);

    parser.addPositionalArgument(QStringLiteral("uids"), i18n("UID(s) of person(s) to show."), QStringLiteral("[uids...]"));

    parser.process(m_serverApp);

    m_aboutData.processCommandLine(&parser);
#endif
    // TODO: check of arguments already here?
//     if( !KUniqueApplication::start() )
//         return 0; //TODO: send arguments to running instance?
    KDBusService programDBusService(KDBusService::Unique);

    Kontinga::CardWindowManager* Server = new Kontinga::CardWindowManager;

    new Kontinga::CardWindowManagerDBusAdaptor(Server);
    QDBusConnection connection = QDBusConnection::sessionBus();
    connection.registerObject("/org/kde/kontinga/CardWindowManager", Server);
    connection.registerService("org.kde.kontinga.CardWindowManager");

#if 0
    // started by session management?
    if (m_serverApp.isSessionRestored() && KMainWindow::canBeRestored(1)) {
        mainWindow->restore(1);
    } else {
        // no session.. just start up normally
        const QStringList urls = parser.positionalArguments();

        // take arguments
        if (!urls.isEmpty()) {
            const QString currentPath = QDir::currentPath();
            for (const QString& url : urls) {
                mDocumentStrategy->load(QUrl::fromUserInput(url, currentPath, QUrl::AssumeLocalFile));
            }
        }
    }
#endif
    return m_serverApp.exec();
}
