/*
    This file is part of the KontingaCards program, part of the KDE project.

    Copyright (C) 2019 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
 */

#ifndef CARDWINDOMANAGERDBUSADAPTOR_HPP
#define CARDWINDOMANAGERDBUSADAPTOR_HPP

// Qt
#include <QDBusAbstractAdaptor>

namespace Kontinga {

class CardWindowManager;

class CardWindowManagerDBusAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.kontinga.CardWindowManager")

public:
    explicit CardWindowManagerDBusAdaptor(CardWindowManager* cardWindowManager);
    ~CardWindowManagerDBusAdaptor() override;

public Q_SLOTS:
    void ShowCard(const QString& uid);
    void CloseCard(const QString& uid);

private:
    CardWindowManager* const m_cardWindowManager;
};

}

#endif
