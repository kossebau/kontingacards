/*
    This file is part of the KontingaCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
 */

#include "cardwindow.hpp"

// KF
#include <KLocalizedString>
#include <KIconLoader>
// Qt
#include <QMenu>
#include <QContextMenuEvent>

static const char ServerContextId[] = "KontingaCards";

static const int PhotoWidthPixels = 50;
static const int PhotoHeightPixels = 70;

namespace Kontinga {

CardWindow::CardWindow(const KContacts::Addressee& person)
    : CardView()
{
    connect(this, &CardView::personChanged, this, &CardWindow::updateView);

    m_servicesMenuFiller.setContext(ServerContextId);

//     setWindowFlags( WType_TopLevel | WStyle_Customize | WStyle_NormalBorder | WDestructiveClose );
    setBackgroundRole(QPalette::Background);

    setPerson(person);
}

CardWindow::~CardWindow()
{
    emit closed(person());
}

void CardWindow::contextMenuEvent(QContextMenuEvent* event)
{
    QMenu* menu = createStandardContextMenu(event->pos());

    m_servicesMenuFiller.set(person());
    m_servicesMenuFiller.fillMenu(menu);

    menu->exec(event->globalPos());

    delete menu;
}

void CardWindow::updateView()
{
    // window frame
    setWindowTitle(person().realName());
    const QImage image = person().photo().data();

    QPixmap photo;
    if (image.isNull()) {
        photo = KIconLoader::global()->loadIcon("personal", KIconLoader::NoGroup, PhotoWidthPixels);
    } else {
        photo = QPixmap::fromImage(image.scaled(PhotoWidthPixels, PhotoHeightPixels, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }

    setWindowIcon(photo);
}

// sigh, sync() does not give the width
// so we have to catch the calculation here and resize to gain a perfect card
void CardWindow::showEvent(QShowEvent* showEvent)
{
    CardView::showEvent(showEvent);
//     resize( contentsWidth(), contentsHeight() );
}

}
