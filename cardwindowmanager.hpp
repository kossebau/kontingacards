/*
    This file is part of the KontingaCards program, part of the KDE project.

    Copyright (C) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
 */

#ifndef CARDWINDOMANAGER_HPP
#define CARDWINDOMANAGER_HPP

// Qt
#include <QSet>
#include <QHash>
#include <QString>
#include <QObject>

namespace KContacts {
class Addressee;
}
class KJob;
class QTimer;

namespace Kontinga {
class CardWindow;

class CardWindowManager : public QObject
{
    Q_OBJECT

public:
    CardWindowManager();
    ~CardWindowManager() override;

public:
    void showCard(const QString& uid);
    void closeCard(const QString& uid);

private Q_SLOTS:
    /** */
    void updateCards();
    /** */
    void removeCard(const KContacts::Addressee& person);

    void contactSearchResult(KJob* job);

private:
    /** holds open cards based on uid */
    QHash<QString, CardWindow*> m_cards;
    QSet<QString> m_pendingContactQueries;

    /** */
    QTimer* m_shutDownTimer;
};

}

#endif
